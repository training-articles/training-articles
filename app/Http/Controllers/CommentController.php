<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Article;
use Illuminate\Http\Request;
 use App\Http\Requests\StoreComment;
//use App\Http\Resources\CommentResource as CommentResource;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Article $article)
    {
        $sortBy = $request->query('sortby') == 'oldest' ? 'ASC' : 'DESC';
        $comments = $article->comments()->orderBy('created_at', $sortBy)->paginate(10);
        return rest_api('OK', $comments);
    }

    public function show(Comment $comment)
    {
        $comment->load('user');
        return rest_api('OK', $comment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $data = $request->only('content', 'article_id');
        $data['user_id'] = auth()->user()->id;
        $comment = Comment::create($data);
        return rest_api('OK', $comment, 200);
    }

    public function destroy($id_comment)
    {
        if ($comment->user_id != auth()->user()->id){
            return rest_error('ERROR');
        }

        $comment->delete();
        return rest_api('OK');
    }
}
