<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticle;
use App\Http\Requests\UpdateArticle;
//use App\Http\Resources\ArticleResource as ArticleResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return rest_api('OK', $articles);
        //return ArticleResource::collection($article);
        // return response()->json(['message' => 'OK', 'data' => $article])
    }

    public function show(Article $article)
    {
        $article->load('comments')->loadCount('comments');
        return rest_api('OK', $article);
        //return new ArticleResource($article);
    }

    public function store(StoreArticle $request)
    {
        $data = $request->only('title', 'content', 'category_id');
        $data['user_id'] = auth()->user()->id;
        $article = Article::create($data);
        return rest_api('OK', $article);
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return rest_api('OK');
    }

    public function update(UpdateArticle $request, Article $article)
    {
        $data = $request->only('title', 'content', 'category_id');
        $article->update($data);
        return rest_api('OK', $article);
    }
}
