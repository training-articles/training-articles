<?php

namespace App\Http\Controllers;

//use Exception;
use App\Category;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;
use Illuminate\Http\Request;
//use App\Http\Resources\CategoryResource as CategoryResource;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return rest_api('OK', $categories);
        //return CategoryResource::collection($category);
    }

    public function show(Category $category)
    {
        return rest_api('OK', $category);
        //$category = Category::where('id_categories', $id_categories)->first();
        //return new CategoryResource($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {
        $category = Category::create($request->only('name'));
        return rest_api('OK', $category, 200);
        //return new CategoryResource($request);
        //return response()->json([
        //    'message' => 'Input Category Success'
        //]);
    }

    public function destroy(Category $category)
    {
        //$category = Category::find($id_categories);
        $category->delete();
        //return new CategoryResource($category);
        return rest_api('OK');
    }

    public function update(UpdateCategory $request, Category $category)
    {
        $category->update($request->only('name'));
        return rest_api('OK', $category);
        // $category = Category::find($id_categories);
        // if ($category) {
        //     $category->name = $request->input('name');
        //     $category->update($request->all());
    
        //     return new CategoryResource($category);
        // } 
        // else {
        //     return response()->json([
        //         "message" => "Category not found"
        //     ], 404);
        // }
    }

    // public function trashCategory()
    // {
    //     $category = Category::onlyTrashed()->get();
    //     return view('categorytrash', ['category' => $category]);
    // }
}
