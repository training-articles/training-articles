<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
//use App\Http\Resources\UserResource as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return rest_api('OK', $users);
        //return UserResource::collection($user);
    }

    public function show(User $user)
    {
        return rest_api('OK', $user);
        // $user = User::where('id_user', $id_user)->first();
        // return new UserResource($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $data = $request->only('name', 'email');
        $data['password'] = Hash::make($request->password);

        $user = User::create($data);
        return rest_api('OK', $user, 200);
    }

    public function destroy($id_user)
    {
        $user->delete();
        return rest_api('OK');
    }

    public function update(UpdateUser $request, User $user)
    {
        $data = $request->only('name', 'email');

        if ($request->password) {
            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);
        return rest_api('OK', $user);
    }
}
