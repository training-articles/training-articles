<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id_comment' => $this->id_comment,
            'content' => $this->content,
            'id_user' => $this->id_user,
            'id_articles' => $this->id_articles,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
