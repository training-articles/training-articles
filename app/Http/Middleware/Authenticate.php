<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use JWTAuth;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (TokenInvalidException $e) {
            return response()->json([
                'message' => 'Invalid Token'
            ]);
        } catch (TokenExpiredException $e) {
            return response()->json([
                'message' => 'Expired Token'
            ]);
        } catch (Exception $e){
            return response()->json([
                'message' => 'Token not found'
            ]);
        }
        return $next($request);
    }
}