<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    //Route::post('refresh', 'AuthController@refresh');
    //Route::post('me', 'AuthController@me');
});

Route::group([
    'middleware' => 'auth'
], function ($router){
    Route::prefix('category')->group(function(){
        Route::get('/', 'CategoryController@index');
        Route::get('/id/{id}', 'CategoryController@show');
        Route::delete('/{id}', 'CategoryController@destroy');
        Route::put('/{id}', 'CategoryController@update');
        Route::post('/', 'CategoryController@store');
    });

    Route::prefix('user')->group(function(){
        Route::get('/', 'UserController@index');
        Route::get('/id/{id}', 'UserController@show');
        Route::delete('/{id}', 'UserController@destroy');
        Route::put('/{id}', 'UserController@update');
        Route::post('/', 'UserController@store');
    });

    Route::prefix('article')->group(function(){
        Route::get('/', 'ArticleController@index');
        Route::get('/id/{id}', 'ArticleController@show');
        Route::delete('/{id}', 'ArticleController@destroy');
        Route::put('/{id}', 'ArticleController@update');
        Route::post('/', 'ArticleController@store');
    });

    Route::prefix('comment')->group(function(){
        Route::get('/', 'CommentController@index');
        Route::get('/id/{id}', 'CommentController@show');
        Route::delete('/{id}', 'CommentController@destroy');
        Route::put('/{id}', 'CommentController@update');
        Route::post('/', 'CommentController@store');
    });
});
